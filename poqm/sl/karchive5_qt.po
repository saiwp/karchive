# Matjaž Jeran <matjaz.jeran@amis.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-10-22 08:20+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100>=3 && n"
"%100<=4 ? 2 : 3);\n"
"X-Qt-Contexts: true\n"
"X-Generator: Lokalize 22.08.1\n"

#: k7zip.cpp:2319
msgctxt "K7Zip|"
msgid "Could not get underlying device"
msgstr "Ni mogoče dobiti spodnje naprave"

#: k7zip.cpp:2327
msgctxt "K7Zip|"
msgid "Read header failed"
msgstr "Branje glave ni uspelo"

#: k7zip.cpp:2333
msgctxt "K7Zip|"
msgid "Check signature failed"
msgstr "Preverjanje podpisa ni uspelo"

#: k7zip.cpp:2356
msgctxt "K7Zip|"
msgid "Bad CRC"
msgstr "Slab CRC"

#: k7zip.cpp:2365
msgctxt "K7Zip|"
msgid "Next header size is too big"
msgstr "Velikost naslednja glave je prevelika"

#: k7zip.cpp:2370
msgctxt "K7Zip|"
msgid "Next header size is less than zero"
msgstr "Velikost naslednja glave je manjša od nič"

#: k7zip.cpp:2381
#, qt-format
msgctxt "K7Zip|"
msgid "Failed read next header size; should read %1, read %2"
msgstr ""
"Branje velikosti naslednje glave; bi moralo biti prebrano %1, prebrano %2"

#: k7zip.cpp:2393
msgctxt "K7Zip|"
msgid "Bad next header CRC"
msgstr "Slab CRC naslednje glave"

#: k7zip.cpp:2401
msgctxt "K7Zip|"
msgid "Error in header"
msgstr "Napaka v glavi"

#: k7zip.cpp:2420
msgctxt "K7Zip|"
msgid "Wrong header type"
msgstr "Napačna vrsta glave"

#: k7zip.cpp:2430 k7zip.cpp:2436
msgctxt "K7Zip|"
msgid "Not implemented"
msgstr "Ni izdelano"

#: k7zip.cpp:2442
msgctxt "K7Zip|"
msgid "Error while reading main streams information"
msgstr "Napaka ob branju informacije o glavnih tokovih"

#: k7zip.cpp:2460
msgctxt "K7Zip|"
msgid "Error while reading header"
msgstr "Napaka ob branju glave"

#: k7zip.cpp:2520
msgctxt "K7Zip|"
msgid "Error reading modification time"
msgstr "Napaka ob branju časa spremembe"

#: k7zip.cpp:2566
msgctxt "K7Zip|"
msgid "Error reading MTime"
msgstr "Napaka ob branju MTime"

#: k7zip.cpp:2573
msgctxt "K7Zip|"
msgid "Invalid"
msgstr "Neveljavno"

#: k7zip.cpp:2595
#, qt-format
msgctxt "K7Zip|"
msgid "Read size failed (checkRecordsSize: %1, d->pos - ppp: %2, size: %3)"
msgstr ""
"Branje velikosti ni uspelo  (preverjanje velikosti zapisa: %1, d->pos - ppp: "
"%2, velikost: %3)"

#: k7zip.cpp:2796
msgctxt "K7Zip|"
msgid "Write error"
msgstr "Napaka pisanja"

#: k7zip.cpp:2826
msgctxt "K7Zip|"
msgid "Failed while encoding header"
msgstr "Napaka med kodiranjem glave"

#: k7zip.cpp:2870
msgctxt "K7Zip|"
msgid "No file currently selected"
msgstr "Trenutno ni izbrane nobene datoteke"

#: k7zip.cpp:2894 k7zip.cpp:2944 k7zip.cpp:2986
msgctxt "K7Zip|"
msgid "Application error: 7-Zip file must be open before being written into"
msgstr "Napaka aplikacije: datoteka 7-Zip mora biti odprta pred pisanjem vanjo"

#: k7zip.cpp:2900 k7zip.cpp:2992
msgctxt "K7Zip|"
msgid "Application error: attempted to write into non-writable 7-Zip file"
msgstr "Napaka aplikacije: poskus pisanja v nezapisljivo datoteko 7-Zip"

#: kar.cpp:56 kar.cpp:63 kar.cpp:70 kar.cpp:77
msgctxt "KAr|"
msgid "Cannot write to AR file"
msgstr "Ni mogoče pisati v datoteko AR"

#: kar.cpp:90
#, qt-format
msgctxt "KAr|"
msgid "Unsupported mode %1"
msgstr "Nepodprt način %1"

#: kar.cpp:101
msgctxt "KAr|"
msgid "Invalid main magic"
msgstr "Neveljavna glavna magija"

#: kar.cpp:118
msgctxt "KAr|"
msgid "Invalid magic"
msgstr "Neveljavna magija"

#: kar.cpp:129
msgctxt "KAr|"
msgid "Invalid size"
msgstr "Neveljavna velikost"

#: kar.cpp:149
msgctxt "KAr|"
msgid "Invalid longfilename reference"
msgstr "Neveljavni sklic na dolgo ime datoteke"

#: kar.cpp:153
msgctxt "KAr|"
msgid "Invalid longfilename position reference"
msgstr "Neveljavni sklic na lokacijo dolgega imena datoteke"

#: karchive.cpp:157
msgctxt "KArchive|"
msgid "No filename or device was specified"
msgstr "Ni določenega imena datoteke ali naprave"

#: karchive.cpp:162
#, qt-format
msgctxt "KArchive|"
msgid "Could not open device in mode %1"
msgstr "Ni bilo mogoče odpreti naprave v načinu %1"

#: karchive.cpp:189
#, qt-format
msgctxt "KArchive|"
msgid "QSaveFile creation for %1 failed: %2"
msgstr "Kreiranje QSaveFile za %1 ni uspelo: %2"

#: karchive.cpp:208
#, qt-format
msgctxt "KArchive|"
msgid "Unsupported mode %1"
msgstr "Nepodprt način %1"

#: karchive.cpp:217
msgctxt "KArchive|"
msgid "Archive already closed"
msgstr "Arhiv je že zaprt"

#: karchive.cpp:268
#, qt-format
msgctxt "KArchive|"
msgid "%1 doesn't exist or is not a regular file."
msgstr "%1 ne obstaja ali ni regularna datoteka."

#: karchive.cpp:274
#, qt-format
msgctxt "KArchive|"
msgid ""
"Failed accessing the file %1 for adding to the archive. The error was: %2"
msgstr ""
"Ni mogoče dostopati do datoteke %1 za dodajanje k arhivu. Napaka je bila: %2"

#: karchive.cpp:320
#, qt-format
msgctxt "KArchive|"
msgid "Couldn't open file %1: %2"
msgstr "Ni bilo mogoče odpreti datoteke %1: %2"

#: karchive.cpp:354
#, qt-format
msgctxt "KArchive|"
msgid "Directory %1 does not exist"
msgstr "Imenik %1 ne obstaja"

#: karchive.cpp:418
#, qt-format
msgctxt "KArchive|"
msgid "Writing failed: %1"
msgstr "Pisanje ni uspelo: %1"

#: karchive_p.h:56
msgctxt "KArchivePrivate|"
msgid "Unknown error"
msgstr "Neznana napaka"

#: kcompressiondevice.cpp:485
msgctxt "KCompressionDevice|"
msgid "Could not write. Partition full?"
msgstr "Ni bilo mogoče pisati. Ali je particija polna?"

#: krcc.cpp:83 krcc.cpp:90 krcc.cpp:97 krcc.cpp:104
msgctxt "KRcc|"
msgid "Cannot write to RCC file"
msgstr "Ni mogoče pisati v datoteko RCC"

#: krcc.cpp:117
#, qt-format
msgctxt "KRcc|"
msgid "Unsupported mode %1"
msgstr "Nepodprt način %1"

#: krcc.cpp:124
#, qt-format
msgctxt "KRcc|"
msgid "Failed to register resource %1 under prefix %2"
msgstr "Ni mogoče registrirati vira %1 s predpono %2"

#: ktar.cpp:343
#, qt-format
msgctxt "KTar|"
msgid "File %1 does not exist"
msgstr "Datoteka %1 ne obstaja"

#: ktar.cpp:350
#, qt-format
msgctxt "KTar|"
msgid "Archive %1 is corrupt"
msgstr "Arhiv %1 je pokvarjen"

#: ktar.cpp:354
msgctxt "KTar|"
msgid "Disk full"
msgstr "Disk poln"

#: ktar.cpp:388
msgctxt "KTar|"
msgid "Could not get underlying device"
msgstr "Ni mogoče dobiti spodnje naprave"

#: ktar.cpp:403
msgctxt "KTar|"
msgid "Could not read tar header"
msgstr "Ni mogoče prebrati glave tar"

#: ktar.cpp:581
#, qt-format
msgctxt "KTar|"
msgid "Failed to write back temp file: %1"
msgstr "Ni mogoče zapisati nazaj začasne datoteke: %1"

#: ktar.cpp:637
#, qt-format
msgctxt "KTar|"
msgid "Couldn't write alignment: %1"
msgstr "Ni mogoče zapisati poravnave: %1"

#: ktar.cpp:760 ktar.cpp:837 ktar.cpp:906
msgctxt "KTar|"
msgid "Application error: TAR file must be open before being written into"
msgstr "Napaka aplikacije: TAR mora biti odprta pred pisanjem vanjo"

#: ktar.cpp:766
msgctxt "KTar|"
msgid "Application error: attempted to write into non-writable 7-Zip file"
msgstr "Napaka aplikacije: poskus pisanja v nezapisljivo datoteko 7-Zip"

#: ktar.cpp:821
#, qt-format
msgctxt "KTar|"
msgid "Failed to write header: %1"
msgstr "Ni mogoče zapisati glave: %1"

#: ktar.cpp:843 ktar.cpp:912
msgctxt "KTar|"
msgid "Application error: attempted to write into non-writable TAR file"
msgstr "Napaka aplikacije: poskus pisanja v nezapisljivo datoteko TAR"

#: kzip.cpp:450 kzip.cpp:469
#, qt-format
msgctxt "KZip|"
msgid "Invalid ZIP file. Unexpected end of file. (Error code: %1)"
msgstr ""
"Neveljavna datoteka ZIP. Nepričakovan konec datoteke. (Koda napake: %1)"

#: kzip.cpp:493
msgctxt "KZip|"
msgid "Invalid ZIP file. Negative name length"
msgstr "Neveljavna datoteka ZIP. Negativna dolžina imena"

#: kzip.cpp:498
msgctxt "KZip|"
msgid "Invalid ZIP file. Name not completely read (#2)"
msgstr "Neveljavna datoteka ZIP. Nepopolno prebrano ime (#2)"

#: kzip.cpp:517
msgctxt "KZip|"
msgid "Invalid ZIP File. Broken ExtraField."
msgstr "Neveljavna datoteka ZIP. Pokvarjen ExtraField."

#: kzip.cpp:531 kzip.cpp:554
msgctxt "KZip|"
msgid "Could not seek to next header token"
msgstr "Ni bilo mogoče preiskati do naslednjega žetona glave"

#: kzip.cpp:546
msgctxt "KZip|"
msgid "Invalid ZIP file. Unexpected end of file. (#5)"
msgstr "Neveljavna datoteka ZIP. Nepričakovan konec datoteke. (#5)"

#: kzip.cpp:562
msgctxt "KZip|"
msgid "Could not seek to file compressed size"
msgstr "Ni bilo mogoče preiskati do velikosti stisnjene datoteke"

#: kzip.cpp:578
msgctxt "KZip|"
msgid "Invalid ZIP file. Unexpected end of file. (#1)"
msgstr "Neveljavna datoteka ZIP. Nepričakovan konec datoteke. (#1)"

#: kzip.cpp:611
msgctxt "KZip|"
msgid ""
"Invalid ZIP file, central entry too short (not long enough for valid entry)"
msgstr ""
"Neveljavna datoteka ZIP, osrednji vnos prekratek (ni dovolj dolg za veljavni "
"vnos)"

#: kzip.cpp:620
msgctxt "KZip|"
msgid "Invalid ZIP file, file path name length smaller or equal to zero"
msgstr ""
"Neveljavna datoteka ZIP, dolžina imena poti datoteke manjše ali enaka nič"

#: kzip.cpp:696
msgctxt "KZip|"
msgid "Invalid ZIP file, found empty entry name"
msgstr "Neveljavna datoteka ZIP, najdeno prazno ime vnosa"

#: kzip.cpp:737
#, qt-format
msgctxt "KZip|"
msgid "File %1 is in folder %2, but %3 is actually a file."
msgstr "Datoteka %1 je v mapi %2, ampak %3 je dejansko datoteka."

#: kzip.cpp:748
msgctxt "KZip|"
msgid "Could not seek to next entry"
msgstr "Ni bilo mogoče preiskati do naslednjega vnosa"

#: kzip.cpp:761 kzip.cpp:771
msgctxt "KZip|"
msgid "Invalid ZIP file. Unexpected end of file."
msgstr "Neveljavna datoteka ZIP. Nepričakovan konec datoteke."

#: kzip.cpp:794
#, qt-format
msgctxt "KZip|"
msgid "Invalid ZIP file. Unrecognized header at offset %1"
msgstr "Neveljavna datoteka ZIP. Neprepoznana glava pri odmiku %1"

#: kzip.cpp:825
#, qt-format
msgctxt "KZip|"
msgid "Could not seek to next file header: %1"
msgstr "Ni bilo mogoče preiskati do naslednje glave datoteke: %1"

#: kzip.cpp:851 kzip.cpp:947
#, qt-format
msgctxt "KZip|"
msgid "Could not write file header: %1"
msgstr "Ni mogoče zapisati glave datoteke: %1"

#: kzip.cpp:994
#, qt-format
msgctxt "KZip|"
msgid "Could not write central dir record: %1"
msgstr "Ni mogoče zapisati osrednjega zapisa imenika: %1"

#: kzip.cpp:1030
msgctxt "KZip|"
msgid "Application error: ZIP file must be open before being written into"
msgstr "Napaka aplikacije: datoteka ZIP mora biti odprta pred pisanjem vanjo"

#: kzip.cpp:1036
msgctxt "KZip|"
msgid "Application error: attempted to write into non-writable ZIP file"
msgstr "Napaka aplikacije: poskus pisanja v nezapisljivo  datoteko ZIP"

#: kzip.cpp:1042
msgctxt "KZip|"
msgid "Cannot create a device. Disk full?"
msgstr "Ni mogoče ustvariti naprave. Disk poln?"

#: kzip.cpp:1048
msgctxt "KZip|"
msgid "Cannot seek in ZIP file. Disk full?"
msgstr "Ni mogoče iskati po datoteki ZIP. Disk poln?"

#: kzip.cpp:1190
msgctxt "KZip|"
msgid "Could not write to the archive. Disk full?"
msgstr "Ni mogoče pisati v arhiv. Disk poln?"

#: kzip.cpp:1209
#, qt-format
msgctxt "KZip|"
msgid "Could not open compression device: %1"
msgstr "Ni mogoče odpreti naprave za stiskanje: %1"

#: kzip.cpp:1296
msgctxt "KZip|"
msgid "No file or device"
msgstr "Ni datoteke ali naprave"

#: kzip.cpp:1309
#, qt-format
msgctxt "KZip|"
msgid "Error writing data: %1"
msgstr "Napaka pri pisanju podatkov: %1"
